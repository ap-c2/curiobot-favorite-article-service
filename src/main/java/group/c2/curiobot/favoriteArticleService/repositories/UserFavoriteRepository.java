package group.c2.curiobot.favoriteArticleService.repositories;

import group.c2.curiobot.favoriteArticleService.models.UserFavorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserFavoriteRepository extends JpaRepository<UserFavorite, Long> {

	List<UserFavorite> findByOwnerUserId(String userId);

	@RestResource(path = "title", rel = "title")
	@Query("FROM UserFavorite fav WHERE LOWER(fav.title) LIKE CONCAT('%', LOWER(:contains), '%')")
	List<UserFavorite> findByTitle(@Param("contains") String title);

	@RestResource(path = "url", rel = "url")
	@Query("FROM UserFavorite fav WHERE LOWER(fav.url) LIKE CONCAT('%', LOWER(:contains), '%')")
	List<UserFavorite> findByUrl(@Param("contains") String url);
}
