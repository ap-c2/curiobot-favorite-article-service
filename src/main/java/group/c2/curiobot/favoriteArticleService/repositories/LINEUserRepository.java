package group.c2.curiobot.favoriteArticleService.repositories;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LINEUserRepository extends JpaRepository<LINEUser, String> {

	Optional<LINEUser> findByUserId(String userId);
}
