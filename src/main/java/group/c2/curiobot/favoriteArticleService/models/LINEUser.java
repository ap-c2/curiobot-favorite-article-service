package group.c2.curiobot.favoriteArticleService.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@NoArgsConstructor
@Table(name = "line_user", uniqueConstraints = {@UniqueConstraint(columnNames = "userId")})
public class LINEUser implements Serializable {

	@Id
	@NotBlank
	@Column(unique = true, nullable = false)
	private String userId;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<UserFavorite> favorites = new ArrayList<>();

	public LINEUser(@NonNull String userId) {
		this.userId = userId;
	}
}
