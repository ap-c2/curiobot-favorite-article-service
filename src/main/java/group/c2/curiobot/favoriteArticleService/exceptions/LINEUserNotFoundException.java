package group.c2.curiobot.favoriteArticleService.exceptions;

public class LINEUserNotFoundException extends RuntimeException {

	public LINEUserNotFoundException(String id) {
		super("Could not find LINE user with id: " + id);
	}
}
