package group.c2.curiobot.favoriteArticleService.controllers;

import group.c2.curiobot.favoriteArticleService.exceptions.LINEUserNotFoundException;
import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.models.UserFavorite;
import group.c2.curiobot.favoriteArticleService.repositories.LINEUserRepository;
import group.c2.curiobot.favoriteArticleService.repositories.UserFavoriteRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class UserFavoriteController {

	private final LINEUserRepository lineUserRepository;

	private final UserFavoriteRepository userFavoriteRepository;

	public UserFavoriteController(UserFavoriteRepository userFavoriteRepository,
	                              LINEUserRepository lineUserRepository) {
		this.lineUserRepository = lineUserRepository;
		this.userFavoriteRepository = userFavoriteRepository;
	}

	@GetMapping("/favorites")
	public List<UserFavorite> getAllFavorites() {
		return userFavoriteRepository.findAll();
	}

	@GetMapping("/users/{userId}/favorites")
	public List<UserFavorite> getFavoritesByUser(@PathVariable String userId) {
		return userFavoriteRepository.findByOwnerUserId(userId);
	}

	@GetMapping(value = "/favorites/search/title")
	public List<UserFavorite> getFavoritesByTitle(@RequestParam("contains") String title) {
		return userFavoriteRepository.findByTitle(title);
	}

	@GetMapping(value = "/favorites/search/url")
	public List<UserFavorite> getFavoritesByUrl(@RequestParam("contains") String url) {
		return userFavoriteRepository.findByUrl(url);
	}

	@PostMapping("/users/{userId}/favorites")
	public UserFavorite saveUserFavorite(@PathVariable String userId,
	                                     @RequestBody UserFavorite userFavorite) {
		try {
			LINEUser owner = lineUserRepository.findByUserId(userId).get();
			userFavorite.setOwner(owner);
			return userFavoriteRepository.save(userFavorite);
		} catch (NoSuchElementException e) {
			throw new LINEUserNotFoundException(userId);
		}
	}
}