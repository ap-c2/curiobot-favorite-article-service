package group.c2.curiobot.favoriteArticleService.controllers;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.repositories.LINEUserRepository;
import group.c2.curiobot.favoriteArticleService.exceptions.LINEUserNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LINEUserController {

	private final LINEUserRepository lineUserRepository;

	public LINEUserController(LINEUserRepository repository) {
		this.lineUserRepository = repository;
	}

	@GetMapping("/users")
	public List<LINEUser> getAllLINEUser() {
		return lineUserRepository.findAll();
	}

	@GetMapping("/users/{id}")
	public LINEUser getLINEUserByUserId(@PathVariable String id) {
		return lineUserRepository.findByUserId(id)
				.orElseThrow(() -> new LINEUserNotFoundException(id));
	}

	@PostMapping("/users")
	public LINEUser saveLINEUser(@RequestBody LINEUser user) {
		if (lineUserRepository.findByUserId(user.getUserId()).isPresent()) {
			return lineUserRepository.findByUserId(user.getUserId()).get();
		} else {
			return lineUserRepository.save(user);
		}
	}

	@DeleteMapping("/users/{id}")
	public void deleteLINEUser(@PathVariable String id) {
		try {
			lineUserRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("LINE user not found");
		}
	}
}
