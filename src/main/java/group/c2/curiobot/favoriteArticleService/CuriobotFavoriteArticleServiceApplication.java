package group.c2.curiobot.favoriteArticleService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuriobotFavoriteArticleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuriobotFavoriteArticleServiceApplication.class, args);
	}

}
