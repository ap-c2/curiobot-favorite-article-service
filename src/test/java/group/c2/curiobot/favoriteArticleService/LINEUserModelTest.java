package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LINEUserModelTest {

	@Autowired
	private TestEntityManager entityManager;

	@Test
	public void saveDummyLINEUser() {
		LINEUser dummyLINEUser = new LINEUser("DummyID");
		LINEUser savedLINEUserData = this.entityManager.persistAndFlush(dummyLINEUser);
		assertThat(savedLINEUserData.getUserId()).isEqualTo("DummyID");
	}
}
