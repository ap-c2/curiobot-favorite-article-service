package group.c2.curiobot.favoriteArticleService.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.MvcResult;

public class ControllerTestUtility {

	private ObjectMapper objectMapper = new ObjectMapper();

	public <T> T fromJsonResult(MvcResult mvcResult,
	                             Class<T> tClass) throws Exception {
		return this.objectMapper.readValue(
				mvcResult.getResponse().getContentAsString(),
				tClass
		);
	}

	public byte[] toJson(Object serializableObject) throws Exception {
		return this.objectMapper.writeValueAsString(serializableObject)
				.getBytes();
	}
}
