package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.repositories.LINEUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LINEUserRepositoryTest {

	@Autowired
	private LINEUserRepository lineUserRepository;

	@Test
	public void saveLINEUserAndFindById() {
		LINEUser firstDummyUser = new LINEUser("dummyid1");
		lineUserRepository.save(firstDummyUser);
		LINEUser queriedUser = lineUserRepository.findByUserId(firstDummyUser.getUserId()).get();
		assertThat(queriedUser.getUserId()).isEqualTo(firstDummyUser.getUserId());
	}

	@Test
	public void saveLINEUserAndDeleteById() {
		LINEUser secondDummyUser = new LINEUser("dummyid2");
		lineUserRepository.save(secondDummyUser);
		lineUserRepository.deleteById("dummyid2");
		assertThat(lineUserRepository.findAll()).doesNotContain(secondDummyUser);
	}
}
