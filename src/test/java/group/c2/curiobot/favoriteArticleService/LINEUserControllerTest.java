package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.utils.ControllerTestUtility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LINEUserControllerTest {

	private static final String BASE_URL = "/users/";

	private static final Logger LOG = LoggerFactory.getLogger(LINEUserControllerTest.class);

	@Autowired
	JdbcTemplate jdbcTemplateObject;

	@Autowired
	private MockMvc mockObject;

	private ControllerTestUtility testUtility = new ControllerTestUtility();

	@Before
	public void setUp() {
		jdbcTemplateObject.execute("DELETE FROM LINE_user; " +
				"INSERT INTO LINE_user(user_id) VALUES ('dummyID');");
	}

	private LINEUser[] getAllUsers() throws Exception {
		LINEUser[] lineUsers = testUtility.fromJsonResult(invokeAllLINEUsers().andReturn(), LINEUser[].class);
		return lineUsers;
	}

	private ResultActions invokeAllLINEUsers() throws Exception {
		return mockObject.perform(get(BASE_URL)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeGetLINEUser(String user_id) throws Exception {
		return mockObject.perform(get(BASE_URL + user_id)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeCreateLINEUser(byte[] userJson) throws Exception {
		return mockObject.perform(post(BASE_URL)
				.content(userJson)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeDeleteLINEUser(String userId) throws Exception {
		return mockObject.perform(delete(BASE_URL + userId));
	}

	@Test
	public void contextLoads() {
		assertThat(jdbcTemplateObject).isNotNull();
		assertThat(mockObject).isNotNull();
	}

	@Test
	public void shouldStartWithOneLINEUserDummyID() throws Exception {
		MvcResult mvcResult = invokeAllLINEUsers()
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].userId", is("dummyID")))
				.andReturn();
		LINEUser[] lineUsers = testUtility.fromJsonResult(mvcResult, LINEUser[].class);
		LOG.debug("Dummy User id: {}", lineUsers[0].getUserId());
	}

	@Test
	public void shouldGetOneLINEUserByGivenID() throws Exception {
		invokeGetLINEUser("dummyID")
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.userId", is("dummyID")))
				.andReturn();
	}

	@Test
	public void shouldCreateLINEUser() throws Exception {
		String userID = "anotherDummy";
		byte[] userJson = testUtility.toJson(new LINEUser(userID));
		invokeCreateLINEUser(userJson)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.userId", is(userID)))
				.andReturn();
	}

	@Test
	public void shouldDeleteLINEUser() throws Exception {
		LINEUser[] lineUsers = getAllUsers();
		assertThat(lineUsers.length).isEqualTo(1);
		String userId = lineUsers[0].getUserId();

		invokeGetLINEUser(userId).andExpect(status().isOk());
		invokeDeleteLINEUser(userId).andExpect(status().isOk());
		assertThat(getAllUsers().length).isEqualTo(0);
	}

	@Test
	public void should404DeleteNotFoundLINEUser() throws Exception {
		invokeDeleteLINEUser("notFound").andExpect(status().isNotFound());
	}
}
