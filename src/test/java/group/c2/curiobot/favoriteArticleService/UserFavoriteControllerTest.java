package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.models.UserFavorite;
import group.c2.curiobot.favoriteArticleService.repositories.LINEUserRepository;
import group.c2.curiobot.favoriteArticleService.utils.ControllerTestUtility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserFavoriteControllerTest {

	private static final String USER_BASE_URL = "/users/";

	private static final String FAVORITE_BASE_URL = "/favorites/";

	private static final Logger LOG = LoggerFactory.getLogger(UserFavoriteControllerTest.class);

	private LINEUserRepository lineUserRepository;

	@Autowired
	private JdbcTemplate jdbcTemplateObject;

	@Autowired
	private MockMvc mockObject;

	private ControllerTestUtility testUtility = new ControllerTestUtility();

	@Before
	public void setUp() {
		jdbcTemplateObject.execute(
				"DELETE FROM LINE_user; " +
				"DELETE FROM User_favorite; " +
				"INSERT INTO LINE_user(user_id) VALUES ('dummyID');" +
				"INSERT INTO User_favorite(title, url, user_id) " +
				"VALUES ('dummyTitle', 'https://dummyURL.com', 'dummyID');");
	}

	private UserFavorite[] getAllFavorites() throws Exception {
		UserFavorite[] userFavorites = testUtility.fromJsonResult(invokeAllUserFavorites().andReturn(),
				UserFavorite[].class);
		return userFavorites;
	}

	private ResultActions invokeAllUserFavorites() throws Exception {
		return mockObject.perform(get(FAVORITE_BASE_URL)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeGetUserFavoritesFromUserWithGivenUserId(String userId) throws Exception {
		return mockObject.perform(get(USER_BASE_URL + userId + FAVORITE_BASE_URL)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeGetUserFavoritesContainingTitle(String title) throws Exception {
		return mockObject.perform(get(FAVORITE_BASE_URL + "/search/title?contains=" + title)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeGetUserFavoritesContainingUrl(String url) throws Exception {
		return mockObject.perform(get(FAVORITE_BASE_URL + "/search/url?contains=" + url)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeCreateUserFavorite(byte[] userFavoriteJson, String userId) throws Exception {
		return mockObject.perform(post(USER_BASE_URL + userId + FAVORITE_BASE_URL)
				.content(userFavoriteJson)
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON));
	}

	private ResultActions invokeDeleteUserFavorite(String userId, Long favoriteId) throws Exception {
		return mockObject.perform(delete(USER_BASE_URL + userId + FAVORITE_BASE_URL + favoriteId));
	}

	@Test
	public void contextLoads() {
		assertThat(jdbcTemplateObject).isNotNull();
		assertThat(mockObject).isNotNull();
	}

	@Test
	public void shouldStartWithOneUserFavorite() throws Exception {
		invokeAllUserFavorites()
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].title", is("dummyTitle")))
				.andExpect(jsonPath("$[0].url", is("https://dummyURL.com")))
				.andReturn();
	}

	@Test
	public void shouldGetUserFavoritesWithGivenUserID() throws Exception {
		invokeGetUserFavoritesFromUserWithGivenUserId("dummyID")
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].title", is("dummyTitle")))
				.andExpect(jsonPath("$[0].url", is("https://dummyURL.com")))
				.andReturn();
	}

	@Test
	public void shouldGetOneUserFavoriteWithGivenTitle() throws Exception {
		invokeGetUserFavoritesContainingTitle("dummyTitle")
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andReturn();
	}

	@Test
	public void shouldGetOneUserFavoriteWithGivenURL() throws Exception {
		invokeGetUserFavoritesContainingUrl("dummyURL")
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andReturn();
	}

	@Test
	public void shouldCreateUserFavorite() throws Exception {
		String articleTitle = "new title";
		String articleURL = "https://example.com";
		String sql = "SELECT * FROM LINE_user WHERE line_user.user_id = 'dummyID'";
		LINEUser lineUser = jdbcTemplateObject.queryForObject(sql, LINEUser.class);
		UserFavorite newUserFavorite = new UserFavorite(articleTitle, articleURL, lineUser);
		byte[] userFavoriteJson = testUtility.toJson(newUserFavorite);
		invokeCreateUserFavorite(userFavoriteJson, lineUser.getUserId())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is(articleTitle)))
				.andExpect(jsonPath("$.url", is(articleURL)))
				.andReturn();
	}
}
