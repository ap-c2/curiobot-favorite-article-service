package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.models.UserFavorite;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserFavoriteModelTest {

	@Autowired
	private TestEntityManager entityManager;

	private LINEUser firstUser;

	@Before
	public void setUp() {
		firstUser = new LINEUser("123");
	}

	@Test
	public void saveDummyUserFavorite() {
		LINEUser savedLINEUser = this.entityManager.persistAndFlush(firstUser);
		UserFavorite userFavorite = new UserFavorite("title", "http://google.com", savedLINEUser);
		UserFavorite savedUserFavorite = this.entityManager.persistFlushFind(userFavorite);
		assertThat(savedUserFavorite.getTitle()).isEqualTo("title");
		assertThat(savedUserFavorite.getUrl()).isEqualTo("http://google.com");
		assertThat(savedUserFavorite.getOwner().getUserId()).isEqualTo("123");
	}
}
