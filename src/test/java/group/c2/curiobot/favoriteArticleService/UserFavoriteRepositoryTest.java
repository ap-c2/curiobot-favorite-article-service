package group.c2.curiobot.favoriteArticleService;

import static org.assertj.core.api.Assertions.assertThat;

import group.c2.curiobot.favoriteArticleService.models.LINEUser;
import group.c2.curiobot.favoriteArticleService.models.UserFavorite;
import group.c2.curiobot.favoriteArticleService.repositories.LINEUserRepository;
import group.c2.curiobot.favoriteArticleService.repositories.UserFavoriteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserFavoriteRepositoryTest {

	@Autowired
	private UserFavoriteRepository userFavoriteRepositoryTest;

	@Autowired
	private LINEUserRepository lineUserRepository;

	private LINEUser dummyUser;

	@Before
	public void setUp() {
		dummyUser = new LINEUser("dummyID");
		dummyUser = lineUserRepository.save(dummyUser);
	}

	@Test
	public void saveArticleAndFindByOwnerUserId() {
		UserFavorite dummyArticle = new UserFavorite("Title", "https://google.com", dummyUser);
		userFavoriteRepositoryTest.save(dummyArticle);
		List<UserFavorite> userFavorites = userFavoriteRepositoryTest.findByOwnerUserId(dummyUser.getUserId());
		assertThat(userFavorites).contains(dummyArticle);
	}

	@Test
	public void saveArticleAndFindByTitle() {
		UserFavorite dummyArticle = new UserFavorite("Title", "https://google.com", dummyUser);
		userFavoriteRepositoryTest.save(dummyArticle);
		List<UserFavorite> userFavorites = userFavoriteRepositoryTest.findByTitle("Title");
		assertThat(userFavorites).contains(dummyArticle);
	}

	@Test
	public void saveArticleAndFindByUrl() {
		UserFavorite dummyArticle = new UserFavorite("Title", "https://google.com", dummyUser);
		userFavoriteRepositoryTest.save(dummyArticle);
		List<UserFavorite> userFavorites = userFavoriteRepositoryTest.findByUrl("https://google.com");
		assertThat(userFavorites).contains(dummyArticle);
	}
}
